# Examen Primera Evaluación Diseño Web y Desarrollo Cliente

# Día 02/12/2021 Tiempo: 5 horas

- Nota: Cada pregunta se valorará como bien o como mal (valoraciones intermedias serán excepcionales).
- Nota2: En cada pregunta se especifica si se valora en el examen de diseño o en el de desarrollo.
- Nota3: Para aprobar cada examen hay que obtener una puntuación mínima de 5 puntos en ese examen.
- Nota4: Organice su tiempo. Si no consigue resolver un apartado pase al siguiente. El examen consta de apartados de diseño y de desarrollo que se pueden resolver por separado. Si un apartado depende de otro que no sabe resolver, siempre puede dar una solución que aunque no sea correcta, le permita seguir avanzando.
- Nota5: Para que una solución sea correcta, no sólo hay que conseguir que haga lo que se pide, sino que además **todo lo que funcionaba lo tiene que seguir haciendo**. La solución debe estar implementada según las prácticas de código limpio explicadas en clase. Esto incluye JavaScript, CSS y HTML.
- Nota6: Lea completamente el examen antes de empezar y comience por lo que le parezca más fácil.
- Nota7: No se permite utilizar ninguna librería que no esté ya incluida en el fichero "package.json". Tampoco se permite usar en CSS Flexbox o Grid Layout.

Pasos previos antes de empezar

- Clone el repositorio del enunciado

```bash
git clone https://gitlab.com/surtich/enunciado-examen-primera-2021-2022.git enunciado-examen-primera
```

- Vaya al directorio del proyecto

```bash
cd enunciado-examen-primera
```

- Configure su usuario de git:

```bash
git config user.name "Sustituya por su nombre y apellidos"
git config user.email "Sustituya por su correo electrónico"
```

- Cree un _branch_ con su nombre y apellidos separados con guiones (no incluya mayúsculas, acentos o caracteres no alfabéticos, excepción hecha de los guiones). Ejemplo:

```bash
    git checkout -b fulanito-perez-gomez
```

- Compruebe que está en la rama correcta:

```bash
    git status
```

- Suba la rama al repositorio remoto:

```bash
    git push origin <nombre-de-la-rama-dado-anteriormente>
```

- Instale las dependencias y arranque la aplicación:

```bash
    yarn install
    yarn start
```

Navegue a [http://localhost:8080](http://localhost:8080)


Debería ver esto:

![Image 0](./img/0.png)


- Dígale al profesor que ya ha terminado para que compruebe que todo es correcto y desconecte la red.


## OBJETIVO

Se pretende que se pueda interpretar una partitura en dos modos.

Modo reproducción:

![Image play](./img/play.gif)


Modo aprendizaje:

![Image learn](./img/learn.gif)


## EXAMEN DE DISEÑO

#### 1.- (2 puntos) Elimine el directorio "css" y cree otro llamado "sass" que contenga todo el diseño de la aplicación. La estructura de directorios será similar a la vista en clase. Habrá un fichero "main.scss" que importará todo el código de *SASS*. Este fichero, "main.scss", será la única importación de estilos en el fichero "index.js". Reorganice el código "css" para que use todas las técnicas de *SASS* (anidación, nombre de variables, ...).

Nota: El resto del código de diseño debe estar escrito en SASS y seguir todo lo dicho en este apartado.

Nota2: Vea la imagen:

![Image 1](./img/1.png)

#### 2.- (2 puntos) La selección de octavas tendrá el estilo mostrado en la imagen. Es decir, que las teclas que no pertenezcan a la octava seleccionada se mostrarán oscurecidas.

![Image 2](./img/2.png)

#### 3.- (2 puntos) La teclas cabrán en todo momento en la pantalla sin hacer *zoom*, y crecerán de tamaño o se contraerán según haya más o menos espacio. Observe que las teclas negras también varían su tamaño y están en todo momento colocadas dónde corresponde.

![Image 3](./img/3.gif)

#### 4.- (2 puntos) Algunas octavas se ocultarán en los puntos de corte 900px y 600px. Observe la octava seleccionada por defecto y las octavas que hay que ocultar en cada caso.

![Image 4](./img/4.gif)

#### 5.- (2 puntos) Las teclas estarán en todo momento centradas horizontal y verticalmente respecto al piano independientemente del tamaño de la pantalla.

![Image 5](./img/5.png)

## EXAMEN DE DESARROLLO

### Play mode

Preparación: añada los botones "learn" y "end" tal y cómo se muestra en la imagen. El boton "end" estará inicialmente oculto. El botón "learn" inicialmente estará visible y activo.

![Image prep](./img/prep.png)


Preparación2: Inicialmente sólo estarán visibles los botones "play" y "learn".

![Image prep2](./img/prep2.png)

Preparación3 (ya implementado). Al pulsar sobre "play":
- Se desactivará "play".
- Se mostrará activado "stop".
- Se empezará a reproducir la partitura. Después de un "stop", la partitura continuará donde se paró al pulsar otra vez "play".

Preparación4 (ya implementado). Al pulsar sobre "stop":  
- Se desactivará "stop".
- Se mostrará activado "play".
- Se parará de reproducir la partitura.

Preparación5 (ya implementado). Al terminar la partitura:
- Se ocultará "stop".
- Se mostrará activado "play".
- Ninguna tecla de la partitura tendrá fondo verde.
#### 6.- (0,25 puntos) Al pulsar sobre "play".
- Se activará y visualizará el botón "end".
- Se desactivará el botón "learn".

#### 7.- (0,25 puntos) Al pulsar sobre "stop".
- Se desactivará el botón "end".

#### 8.- (0,25 puntos) Al terminar la partitura.
- Se ocultará el botón "end".
- Se activará el botón "learn".
#### 9.- (0,5 puntos) Al reproducir la partitura.
- Se seleccionará la octava a la que corresponda la nota que se está reproduciendo en cada momento (ver "play mode").

#### 10.- (0,5 puntos) Al pulsar "stop".
- Se parará la reproducción de la partitura, mostrando la última nota reproducida en verde. Cuando la partitura termine, ninguna nota estará pintada de verde.

![Image 10](./img/10.gif)

#### 11.- (0,25 puntos) Al pulsar "end".
- Se activarán los botones "play" y "learn".
- Se ocultarán los botones "stop" y "end".

#### 12.- (0,5 puntos) Al pulsar "end".
- Ninguna nota estará pintada de verde.
- Al pulsar "play" la partitura comenzará a reproducirse desde la primera nota.

![Image 11](./img/11.gif)

### Learn Mode
#### 13.- (0,25 puntos) Al pulsar "learn".
- Se desactivarán los botones "play" y "learn".
- Se mostrará inactivo el botón "stop".
- Se mostrará activo el botón "end".

#### 14.- (1 punto) Al pulsar "learn".
- Se mostrará la primera nota de la partitura con fondo amarillo.
- Se iniciará una cuenta regresiva que irá de 5 a 0 actualizándose cada segundo.
- Se seleccionará la octava que corresponda a la nota.

![Image 14](./img/14.gif)

#### 15.- (1,25 puntos) Cuando termine la cuenta regresiva de una nota.
- Se pondrá el fondo en rojo de la nota que ha terminado su cuenta.
- La nota mostrará únicamente su valor (no se mostrará la cuenta regresiva).
- Se iniciará la cuenta regresiva de la siguiente nota.

![Image 15](./img/15.gif)

#### 16.- (0,5 puntos) Cuando termine la cuenta regresiva de todas las notas.
- Se mantendrá el fondo coloreado de las notas.
- Se ocultarán los botones "stop", "end".
- Se mostrarán activos los botones "play" y "learn".

#### 17.- (0,5 puntos) Después de terminar el aprendizaje, al pulsar sobre "play" o sobre "learn".
- Se limpiará el color del fondo del aprendizaje anterior.
  
![Image 17](./img/17.gif)

#### 18.- (2 puntos) Durante el aprendizaje, si se pulsa **con el ratón** sobre la tecla correcta.
- El fondo se pondrá en verde.
- La cuenta regresiva finalizará inmediatamente (no se mostrará el contador) y comenzará en la siguiente nota.
  
![Image 18](./img/18.gif)

#### 19.- (2 puntos) Si durante el aprendizaje se pulsa sobre el botón "end".
- Se finalizará inmediatamente la cuenta regresiva de la nota que se esté reproduciendo en ese momento.
- Se hará la cuenta regresiva de todos las notas restantes a toda velocidad.
- Durante este proceso los cuatro botones estarán visibles pero desactivados.
- Cuando la cuenta regresiva de todas las notas termine, se ocultarán los botones "stop" y "end" y se visualizarán  y estarán activos "play" y "learn".
  
![Image 19](./img/19.gif)


## Para entregar

- Ejecute el siguiente comando para comprobar que está en la rama correcta y ver los ficheros que ha cambiado:

```bash
    git status
```

- Prepare los cambios para que se añadan al repositorio local:

```bash
    git add .
    git commit -m "completed exam"
```

- Compruebe que no tiene más cambios que incluir:

```bash
    git status
```

- Dígale al profesor que va a entregar el examen.

- Conecte la red y ejecute el siguiente comando:

```bash
    git push origin <nombre-de-la-rama>
```

- Abandone el aula en silencio.