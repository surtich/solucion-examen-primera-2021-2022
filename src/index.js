import "../sass/main.scss";
import musicSheet from "../music-sheets/opus.txt"

import { addPianoListernes, octaveSelection } from "./piano.js";
import { readMusicSheet, playMusicSheet, learnMusicSheet } from "./music-sheet.js";

octaveSelection(4);
addPianoListernes();
readMusicSheet(musicSheet);

const play = document.getElementById("play");
const stop = document.getElementById("stop");
const end = document.getElementById("end");
const learn = document.getElementById("learn");
const file = document.getElementById("file");

let stopfn = function () {

};

play.onclick = function (event) {
    learn.disabled = true;
    play.disabled = true;
    stop.style.visibility = "visible";
    stop.disabled = false;
    end.style.visibility = "visible";
    end.disabled = false;
    stopfn = playMusicSheet(function () {
        play.disabled = false;
        learn.disabled = false;
        stop.style.visibility = "hidden";
        end.style.visibility = "hidden";
    });
    event.preventDefault();
}

stop.onclick = function (event) {
    play.disabled = false;
    stop.disabled = true;
    end.disabled = true;
    stopfn();
    event.preventDefault();
}

end.onclick = function (event) {
    play.disabled = true;
    stop.disabled = true;
    end.disabled = true;
    learn.disabled = true;
    stopfn(true);
    event.preventDefault();
}


learn.onclick = function (event) {
    stopfn = learnMusicSheet(function () {
        play.disabled = false;
        learn.disabled = false;
        stop.style.visibility = "hidden";
        end.style.visibility = "hidden";
    });
    learn.disabled = true;
    play.disabled = true;
    stop.disabled = true;
    stop.style.visibility = "visible";
    end.disabled = false;
    end.style.visibility = "visible";
    event.preventDefault();
}


file.addEventListener("change", handleFiles, false);

function handleFiles() {
    const fileList = this.files; /* now you can work with the file list */

    const file = fileList[0];
    const reader = new FileReader();

    reader.onload = function () {
        readMusicSheet(reader.result);
    }

    reader.readAsText(file);
}
