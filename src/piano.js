export const musicNotes = ["C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"];

export let octaveSelected = 4;


export function addPianoListernes() {

    document.body.onkeydown = function (event) {
        if (event.key >= "0" && event.key <= "8") {
            octaveSelection(parseInt(event.key));
        } else {
            pulseKey(event, true);
        }
    }

    document.body.onkeyup = function (event) {
        pulseKey(event, false);
    }

}

export function octaveSelection(octave) {
    const octavesDOM = document.getElementsByClassName("octave");

    octavesDOM[octaveSelected].classList.remove("selected");
    octavesDOM[octave].classList.add("selected");

    octaveSelected = octave;
}

function pulseKey(event, isDown) {
    const octave = octaveSelected;

    let musicNote = event.key.toUpperCase();
    if (event.shiftKey) {
        musicNote += "#";
    }

    if (!musicNotes.includes(musicNote)) {
        return;
    }
    const key = musicNote + octave;
    playKey(key, isDown);
}

export function playKey(key, isDown) {
    const keyDOM = document.querySelector(`[data-key='${key}']`);
    if (isDown) {
        keyDOM.classList.add("active");
    } else {
        keyDOM.classList.remove("active");
    }
}