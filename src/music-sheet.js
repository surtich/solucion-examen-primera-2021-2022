import { playKey } from "./piano.js";
import { octaveSelection } from "./piano.js";


export function readMusicSheet(musicSheet) {
    const musicSheetDOM = document.getElementById("music-sheet");
    musicSheetDOM.classList.add("music-sheet");
    const keys = musicSheet.split("\n");

    keys.forEach(key => {
        const keyDOM = document.createElement("li");
        keyDOM.classList.add("music-sheet-key");
        keyDOM.innerHTML = key;

        musicSheetDOM.appendChild(keyDOM);

    });
}

function clean() {
    const musicSheetDOM = document.getElementById("music-sheet");
    const musicSheetKeysDOM = musicSheetDOM.childNodes;
    musicSheetKeysDOM.forEach(musicSheetKeyDOM => {
        musicSheetKeyDOM.classList = "music-sheet-key";
    })
}


let actualKeyIndex = 0;
let previousMusicSheetKeyDOM = null;
export function playMusicSheet(callback) {
    clean();
    const musicSheetDOM = document.getElementById("music-sheet");
    const musicSheetKeysDOM = musicSheetDOM.childNodes;

    const interval = setInterval(function () {
        if (previousMusicSheetKeyDOM) {
            previousMusicSheetKeyDOM.classList.remove("play");
        }

        const musicSheetKeyDOM = musicSheetKeysDOM[actualKeyIndex];
        previousMusicSheetKeyDOM = musicSheetKeyDOM;
        musicSheetKeyDOM.classList.add("play");
        const key = musicSheetKeyDOM.innerText;
        const octave = key.slice(-1);
        octaveSelection(octave);
        playKey(key, true);

        setTimeout(function () {
            playKey(key, false);
        }, 450);

        actualKeyIndex++;

        if (actualKeyIndex >= musicSheetKeysDOM.length) {
            if (previousMusicSheetKeyDOM) {
                previousMusicSheetKeyDOM.classList.remove("play");
            }
            clearInterval(interval);
            actualKeyIndex = 0;
            callback();
            return;
        }
    }, 500);


    return function stop(end) {
        if (end) {
            actualKeyIndex = 0;
            if (previousMusicSheetKeyDOM) {
                previousMusicSheetKeyDOM.classList.remove("play");
            }
            callback();
        }
        clearInterval(interval);
    };

}

let learnTime = 1000;
let learnEnd = false;

export function learnMusicSheet(callback) {
    clean();
    learnTime = 1000;
    learnEnd = false;
    learnKey(0, callback);
    return function () {
        learnTime = 10;
        learnEnd = true;
    }

}


function learnKey(actualLearnKeyIndex, callback) {

    let learnInterval = null;
    const musicSheetDOM = document.getElementById("music-sheet");
    const musicSheetKeysDOM = musicSheetDOM.childNodes;

    if (actualLearnKeyIndex >= musicSheetDOM.childNodes.length) {
        callback();
        return;
    }
    const musicSheetKeyDOM = musicSheetKeysDOM[actualLearnKeyIndex];

    const key = musicSheetKeyDOM.innerText;
    const keyDOM = document.querySelector(`[data-key='${key}']`);

    function check(result) {
        clearInterval(learnInterval);
        musicSheetKeyDOM.innerText = key;
        musicSheetKeyDOM.classList.remove("learn");
        musicSheetKeyDOM.classList.add(result ? "success" : "fail");
        keyDOM.removeEventListener("mousedown", checkOK);
        learnKey(actualLearnKeyIndex + 1, callback);
    }

    function checkOK() {
        check(true);
    }

    keyDOM.addEventListener("mousedown", checkOK);
    musicSheetKeyDOM.classList.add("learn");
    const octave = key.slice(-1);
    octaveSelection(octave);

    let seconds = 5;
    musicSheetKeyDOM.innerText = key + "\t" + seconds;
    learnInterval = setInterval(function () {
        if (learnEnd) {
            learnEnd = false;
            check(false);
            return;
        }
        seconds = seconds - 1;
        musicSheetKeyDOM.innerText = key + "\t" + seconds;
        if (seconds == 0) {
            check(false);
        }
    }, learnTime);

}